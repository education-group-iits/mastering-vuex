import BeerService from '@/services/BeerService'

export default {
    namespaced: true,

    state: {
        beers: [],
        selectedBeerId: null
    },

    mutations: {
        SET_BEERS(state, beers) {
            state.beers = beers
        },

        SET_SELECTED_BEER_ID(state, id) {
            state.selectedBeerId = id
        },
    },

    actions: {
        async fetchBeers({commit}, {page, itemsPerPage}) {
            const beers = await BeerService.getBeers(page, itemsPerPage)
            commit('SET_BEERS', beers)
        },

        setSelectedBeerId({commit}, id) {
            commit('SET_SELECTED_BEER_ID', id)
        }
    },

    getters: {
        getSelectedBeer: (state) => {
            return state.beers.find(x => x.id === state.selectedBeerId)
        }
    }
}

