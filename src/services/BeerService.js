import axios from 'axios'

const apiClient = axios.create({
    baseURL: 'https://api.punkapi.com/v2/'
})

export default {
    async getBeers(page, itemsPerPage) {
        const {data} = await apiClient.get(`beers?page=${page}&per_page=${itemsPerPage}`)
        return data
    }
}
